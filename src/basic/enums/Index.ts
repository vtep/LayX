export const enum Direction {
    LEFT = "left",
    RIGHT = "right",
    TOP = "top",
    BOTTOM = "bottom",
    LEFT_TOP = "left-top",
    RIGHT_TOP = "right-top",
    LEFT_BOTTOM = "left-bottom",
    RIGHT_BOTTOM = "right-bottom"
}

export const enum WindowAnimate {
    NONE = "none",
    ZOOM = "zoom"
}

export const enum WindowMode {
    EMBED = "embed",
    LAYER = "layer",
}

export const enum WindowOffset {
    LEFT_TOP = "leftTop",
    LEFT_CENTER = "leftCenter",
    LEFT_BOTTOM = "leftBottom",
    TOP_CENTER = "topCenter",
    CENTER = "center",
    BOTTOM_CENTER = "bottomCenter",
    RIGHT_TOP = "rightTop",
    RIGHT_CENTER = "rightCenter",
    RIGHT_BOTTOM = "rightBottom"
}

export const enum WindowStatus {
    NONE = "none",
    NORMAL = "normal",
    MAX = "max",
    MIN = "min"
}

export const enum ComponentType {
    ICON = "icon",
    WINDOW_ICON = "window-icon",
    ACTION_BUTTON = "action-button",
    PARCLOSE = "parclose",
    RESIZE_BAR = "resize-bar",
    ACTION_BAR = "action-bar",
    ACTION_BUTTONS = "action-buttons",
    CONTEXT_MENU_BUTTON = "context-menu-button",
    CONTEXT_MENU_BAR = "context-menu-bar",
    CONTEXT_MENU_BUTTONS = "context-menu-buttons",
    TITLE_BAR = "title-bar",
    TAB_BAR = "tab-bar",
    TOOL_BAR = "tool-bar",
    WINDOW = "window",
    MORE_ACTION_CONTEXT_MENU_BAR = "more-action-context-menu-bar",
    TOP_MENU_BAR = "top-menu-bar",
    TOP_MENU_BUTTON = "top-menu-button",
    TOP_MENU_BUTTONS = "top-menu-buttons",
    WINDOW_ICON_CONTEXT_MENU_BAR = "window-icon-context-menu-bar",
    SALVER = "salver",
    SALVER_BAR = "salver-bar",
    SALVER_BUTTON = "salver-button",
    CONTENT = "content",
    CONTENT_PENETRATE = "content-penetrate",
    CONTENT_CONTAINER = "content-container",
    HTML_CONTENT = "html-content",
    URL_CONTENT = "url-content",
    NOTICE = "notice",
    DRAG_LAYER = "drag-layer",
    STATU_BAR = "statu-bar"
}

export const enum WindowContentType {
    HTML = "html",
    LOCAL_URL = "local-url",
    NON_LOCAL_URL = "non-local-url"
}

export const enum NoticeType {
    INFO = "info",
    SUCCESS = "success",
    WARNING = "warning",
    ERROR = "error"
}